import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Button from "../components/Button";
import { storiesOf } from "@storybook/react-native";

storiesOf("Button", module)
  .add("Big Btn", () => (
  <Button
      text="Big Btn"
      color={"red"}
      onPress={() => console.log("un-favorited!")}
      width={300}
      height={100}
  />
  ))
  .add("Small Btn", () => (
  <View style={styles.story}>
    <Text>Small btn with green color</Text>
    <Button
      text="Small Btn"
      color={"green"}
      onPress={() => console.log("favorited!")}
      width={150}
      height={50}
    />
    <Text>Small btn with yellow color</Text>
    <Button
        text="Small Btn"
        color={"yellow"}
        onPress={() => console.log("favorited!")}
        width={100}
        height={30}
    />
  </View>
));

const styles = StyleSheet.create({
  story: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});