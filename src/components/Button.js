import React from "react";
import PropTypes from "prop-types";
import { TouchableOpacity, Text } from "react-native";

const IconButton = ({ 
    width, 
    height, 
    color, 
    text, 
    onPress 
}) => {
  return (
    <TouchableOpacity onPress={onPress} style={{ ...styles.button, backgroundColor: color, width, height }}>
        <Text>{text}</Text>
    </TouchableOpacity>
  );
};

const styles = {
  button: {
    alignSelf: "center",
    marginTop: 10,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  }
};

IconButton.propTypes = {
  color: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
};

export default IconButton;